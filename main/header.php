<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet" >
    <link href="../css/bootstrap-icons-1.7.0/bootstrap-icons.css" rel="stylesheet" >
    <title>Page d'Accueil</title>

    <html>
<head>
<link rel="stylesheet" type="text/css" href="select_style.css">
<script type="text/javascript" src="../js/jquery.js"></script>
</head>
  </head>
  <body  class ="text-white" style="background-repeat: no-repeat; background-color:#18191a; ">
  <nav class="navbar navbar-expand-lg navbar-dark" style="background-repeat: no-repeat; background-color:#242526;">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php"><img src="../icon/logo.png" alt="logo home" width="100px"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
      <li class="nav-item">
          <a <?php if(($_GET['page'])=='deppartement')echo 'class="nav-link active"';    ?>class="nav-link" href="?page=deppartement">Departements</a>
        </li>
        <li class="nav-item">
          <a <?php if(($_GET['page'])=='options')echo 'class="nav-link active"';    ?>class="nav-link" href="?page=options">Options</a>
        </li>
        <li class="nav-item">
          <a <?php if(($_GET['page'])=='elements')echo 'class="nav-link active"';    ?>class="nav-link" href="?page=elements">Elements</a>
        </li>
        <li class="nav-item">
          <a <?php if(($_GET['page'])=='professeur')echo 'class="nav-link active"';    ?>class="nav-link" href="?page=professeur">Professeurs</a>
        </li>
        <li class="nav-item">
          <a <?php if(($_GET['page'])=='etudiant')echo 'class="nav-link active"';    ?>class="nav-link" href="?page=etudiant">Etudiants</a>
        </li>
        <li class="nav-item">
          <a <?php if(($_GET['page'])=='evaluer')echo 'class="nav-link active"';    ?>class="nav-link" href="?page=evaluer">Notes</a>
        </li>
        
      </ul>
      <ul class="navbar-nav  me-2">
      <li class="nav-item">
          <a class="nav-link" href="../main/deconnecter.php">Deconnexion</a>
        </li>
        </ul>
    </div>
  </div>
</nav>