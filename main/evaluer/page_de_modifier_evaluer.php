<!doctype html>
<html lang="en" dir="ltr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap CSS -->
  <link href="../../css/bootstrap.css" rel="stylesheet">
  <link href="../css/bootstrap-icons-1.7.0/bootstrap-icons.css" rel="stylesheet">
  <title>Page d'Accueil</title>
</head>

<body class="text-white" style="background-repeat: no-repeat; background-color:#18191a; ">
  <nav class="navbar navbar-expand-lg navbar-dark" style="background-repeat: no-repeat; background-color:#242526;">
    <div class="container-fluid">
      <a class="navbar-brand" href="../index.php"><img src="../../icon/logo.png" alt="logo home" width="100px"></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="../index.php?page=deppartement">Departements</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../index.php?page=options">Options</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../index.php?page=elements">Elements</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../index.php?page=professeurs">Professeurs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../index.php?page=etudiant">Etudiants</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="../index.php?page=evaluer">Notes</a>
          </li>

        </ul>
        <ul class="navbar-nav  me-2">
          <li class="nav-item">
            <a class="nav-link" href="../deconnecter.php">Deconnexion</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>


  <?php
session_start();
if (isset($_GET['modifier']) && isset($_GET['modifier1'])) {
    $conn = mysqli_connect('localhost', $_SESSION['username'], $_SESSION['password'], $_SESSION['db']);
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    } else {
        $result = mysqli_query($conn, 'select * from evaluer  where codeElem = '.$_GET['modifier'].' and codeEtud = '.$_GET['modifier1'].' ;');
        while ($row_dep = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo "
                                    <div class=\"container-fluid mt-5 \">
                                    <div class=\"jumbotron\">
                                    <div class=\"card text-center text-white bg-dark\">
                                    <div class=\"card-header\" style=\"background-color:#242526;\">Modifier Note</div>
                                    <div class=\"card-body\" style=\"background-color:#18191a;\">
                                    <div class=\"row\">
                                      <div class=\"container\">
                                        <div class=\"row\">
                                          <div class=\"col-md-12\">
                                          <form method=\"POST\" action=\"./modifier_evaluer.php\">
                                          <!-- combo-box -->
                                          <div class=\"input-group mb-3\">
                                          <span class=\"input-group-text\" id=\"basic-addon1\">CodeElem</span>
                                          <input type=\"text\" class=\"form-control\" placeholder=\"CodeElem\" name=\"codeElem\" id=\"codeElem\" aria-label=\"CodeElem\" value=\"" . $row_dep['codeElem'] . "\" aria-describedby=\"basic-addon1\" readonly=\"true\">
                                          </div>
                                          <div class=\"input-group mb-3\">
                                          <span class=\"input-group-text\" id=\"basic-addon1\">CodeEtud</span>
                                          <input type=\"text\" class=\"form-control\" placeholder=\"CodeEtud\" name=\"codeEtud\" id=\"codeEtud\" aria-label=\"CodeEtud\" value=\"" . $row_dep['codeEtud'] . "\" aria-describedby=\"basic-addon1\" readonly=\"true\">
                                          </div>
                                       <div class=\"input-group mb-3\">
                                          <span class=\"input-group-text\" id=\"basic-addon1\">note</span>
                                          <input type=\"text\" class=\"form-control\" placeholder=\"note\" name=\"note\"aria-label=\"note\" value=\"" . $row_dep['note'] . "\" aria-describedby=\"basic-addon1\" required>
                                      </div>
                                      <div class=\"input-group mb-3\">
                                      <span class=\"input-group-text\" id=\"basic-addon1\">dateEva</span>
                                      <input type=\"date\" class=\"form-control\" placeholder=\"dateEva\" name=\"dateEva\"aria-label=\"dateEva\" value=\"" . date('Y-m-d', strtotime( $row_dep['dateEva'])) . "\" aria-describedby=\"basic-addon1\" required>
                                      </div>
                                      <div class=\"d-grid gap-2 d-md-flex justify-content-md-end\">
                                         <a class=\"btn btn-secondary me-md-2\" href=\"../index.php?page=evaluer\" type=\"button\">Fermer</a>
                                          <input class=\"btn  btn-primary\" name=\"modifier\" type=\"submit\" value=\"Modifier\">
                                          </div>
                                          </form>
                                  </div>
                                  </div>
                                  </div>
                                  </div>
                                  </div>
                                  <div class=\"card-footer text-muted\" style=\"background-color:#242526;\">
                                  " . $_SESSION['db'] . "
                                  </div>
                                  </div>
                                  </div>
                                  </div>

                              ";
        }
        ?>

  <?php
}
}

mysqli_close($conn);
?>
  <script src="../../js/bootstrap.js"></script>
  <script src="../../js/jquery.js"></script>
</body>

</html>