<script type="text/javascript">
function fetch_select(val)
{
 $.ajax({
 type: 'post',
 url: './evaluer/fetch_element.php',
 data: {
  get_element:val
 },
 success: function (response) {
  document.getElementById("codeElem").innerHTML=response; 
 }
 });
}
</script>
<?php
session_start();
if (isset($_SESSION['connecter']) && $_SESSION['connecter']){
$conn = mysqli_connect('localhost', $_SESSION['username'], $_SESSION['password'],'PROJET_ECOLE_JAVA_SQL',3306);
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
  }
  $sql=null;
  if(isset($_POST['search'])){
  $sql="select * from evaluer where  codeElem = '".$_POST['search']."' OR codeEtud = '".$_POST['search']."' ;";
  }if($sql== null ) {
    $sql='select * from evaluer;';
  }
  $result = mysqli_query($conn,$sql); 
  if(mysqli_num_rows($result)==0){
    $_SESSION['alert']='chercher';
    header('location:./index.php?page=evaluer');
  }
  echo "
  <div class=\"container-fluid mt-5 \">
  <div class=\"jumbotron\">
  <div class=\"card text-center text-white bg-dark\">
  <div class=\"card-header\" style=\"background-color:#242526;\">Notes</div>
  <div class=\"card-body\" style=\"background-color:#18191a;\">
  <h2 class=\"text-center card-title\"></h2>
  <div class=\"row\">
    <div class=\"container\">
      <div class=\"text-center\">
      <div class=\"text-center container-fluid\">
      <button type=\"button\" class=\"btn   d-inline btn-success mb-5\" data-bs-toggle=\"modal\" data-bs-target=\"#ajouter\">      Ajouter Note
    </button>
    <div class=\"col-3 position-relative top-50 start-50 translate-middle\">
    <form class=\"d-flex \" action=\"#\" method=\"POST\" >
    <input class=\"form-control me-2\"  name=\"search\" type=\"search\" placeholder=\"Search\" aria-label=\"Search\">
    <button class=\"btn btn-primary  \" type=\"submit\">Search</button>
  </form>
  </div>
    <!-- Modal -->
    <div class=\"modal fade\" id=\"ajouter\" tabindex=\"-1\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
      <div class=\"modal-dialog modal-dialog-centered\">
        <div class=\"modal-content modal-dark\" style=\"background-color:#242526;\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\" id=\"exampleModalLabel\">Ajouter Note</h5>
            <button type=\"button\" class=\"btn-close btn-close-white\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
          </div>
          <div class=\"modal-body\">
          <form method=\"POST\" action=\"evaluer/ajouter_evaluer.php\">
                    <!-- combo-box -->
          <div class=\"input-group mb-3\">
          <label class=\"input-group-text\" for=\"codeEtud\">codeEtud</label>
          <select class=\"form-select\"  name=\"codeEtud\" id=\"codeEtud\" onchange=\"fetch_select(this.value);\">
            <option>codeEtud...</option>";
            $note_etud=mysqli_query($conn,"select id,nom from etudiant ORDER BY id ASC;");
            while($etudiant =  mysqli_fetch_array($note_etud, MYSQLI_ASSOC)){
              $valetud=$etudiant['id'].' '.$etudiant['nom'];
              $output = strtok($valetud,  ' ');
              echo "<option value=\"".$output."\">".$valetud."</option>
             ";
            }
          echo"
            </select>
          </div>
        <!-- combo-box -->
        <div class=\"input-group mb-3\">
        <label class=\"input-group-text\" for=\"codeElem\">codeElem</label>
        <select class=\"form-select\"  name=\"codeElem\" id=\"codeElem\">
          <option>codeElem...</option>";
   
        echo"
          </select>
        </div>
        <div class=\"input-group mb-3\">
        <span class=\"input-group-text\" id=\"basic-addon1\">note</span>
        <input type=\"text\" class=\"form-control\" name=\"note\" placeholder=\"note\" aria-label=\"note\" aria-describedby=\"basic-addon1\">
        </div>
        <div class=\"input-group mb-3\">
        <span class=\"input-group-text\" id=\"basic-addon1\">dateEva</span>
        <input type=\"date\" class=\"form-control\" name=\"dateEva\" placeholder=\"dateEva\" aria-label=\"dateEva\" aria-describedby=\"basic-addon1\">
        </div>
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-secondary\" data-bs-dismiss=\"modal\">Close</button>
            <input type=\"submit\" class=\"btn btn-success\" name=\"ajouter\"value=\"Ajouter\"/>
          </div>
          </form>
        </div>
      </div>
    </div>
</div>
      <div class=\"row\">
        <div class=\"col-md-2\"></div>
        <div class=\"col-md-12\">
          <table class=\"table\" style=\"border-raduis:0.3rem;\">
            <thead class=\"table text-white\" style=\"background-color:#242526;\">
              <th>codeEtud</th>
              <th>codeElem</th>
              <th>note</th>
              <th>dateEva</th>
              <th>Action</th>
            </thead>
            <tbody class=\"table table-light\">
           
";
  while($row =  mysqli_fetch_array($result, MYSQLI_ASSOC)){  
    echo "<tr><th scope=\"row\" >". $row['codeEtud'] .  "</th><td>". $row['codeElem'] . "</th><td>" . $row['note'] . "</td><td>" . $row['dateEva'] . "</td><td><a href=\"./evaluer/page_de_modifier_evaluer.php?modifier=".$row['codeElem']."&modifier1=".$row['codeEtud']." \" target=\"_self\"  class=\"btn btn-outline-primary\" >Modifier</a> <a href=\"./evaluer/supprimer_evaluer.php?supprimer=".$row['codeElem']."&supprimer1=".$row['codeEtud']."&supprimer2=".$row['dateEva']."\" name=\"supprimer\" class=\"btn btn-outline-danger\">Supprimer</a></td></tr>"; 
  }
  echo " </tbody>
  </table>
</div>
</div>
</div>
</div>
</div>
<div class=\"card-footer text-muted\" style=\"background-color:#242526;\">
".$_SESSION['db']."
</div>
</div>
</div>
</div>



";
}
mysqli_close($conn);

?>