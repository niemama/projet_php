<?php
session_start();
$servername = "localhost";
$username;
$password;
$db='PROJET_ECOLE_JAVA_SQL';
if(isset($_POST['connecter'])){
  $username=$_POST['username'];
  $password=$_POST['password'];
  $conn = mysqli_connect($servername, $username, $password,$db);
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}
else{
  $_SESSION['username']=$username;
  $_SESSION['password']=$password;
  $_SESSION['connecter']=true;
  $_SESSION['first_time']=true;
  $_SESSION['db']=$db;
  mysqli_close($conn);
  header('Location: index.php');
}
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet" >
    <title>Connexion</title>
    <style>
      #main{
        max-width:  480px;
        margin: 10% auto ;
        height: 500px;
      background-color: #242526;
      }
     body{
       background-color: #18191a;
       color: aliceblue;
     }
    </style>
  </head>
  <body>
    
<div class="text-center">
  <form method="POST" action="#">
    <div class="container rounded" id="main">
      <img class="mt-4 mb-4"src="../img/login.png" alt="Connexion" height="105">
      <h1 class="h3 mb-3 font-weight-normal">Connexion</h1>
      <label class="sr-only" for="username">Username</label>
      <input type="text" name="username" id="username" class="form-control mb-3" placeholder="Votre Nom d'utilisateur" required autofocus>
      <label class="sr-only" for="password-login">Mots de Passe</label>
      <input type="password" name="password" class="form-control"  id="password-login" placeholder="**********" required>
      <div class="mt-3">
      <input type="submit" name="connecter" class="btn btn-lg btn-primary btn-block col-12" value="Connexion">
          </div>
          <div class="form-switch mt-3 container-fluid ">
            <input class="form-check-input" type="checkbox" id="souvenir" >
           <label  for="souvenir" class=" form-check-label">Se souvenir de moi</label>
          </div>
   
    </div>
 </form>
 </div>
      
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="../js/bootstrap.js"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
     -->
  </body>
</html>