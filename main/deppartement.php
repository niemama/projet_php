<?php
session_start();
if (isset($_SESSION['connecter']) && $_SESSION['connecter']){
$conn = mysqli_connect('localhost', $_SESSION['username'], $_SESSION['password'],'PROJET_ECOLE_JAVA_SQL',3306);
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
  }
  $sql=null;
if(isset($_POST['search'])){
$sql="select * from deppartement where  nom like '%".$_POST['search']."%' OR chef = '%".$_POST['search']."%';";
}if($sql== null ) {
  $sql='select * from deppartement;';
}
$result = mysqli_query($conn,$sql); 
if(mysqli_num_rows($result)==0){
  $_SESSION['alert']='chercher';
  header('location:./index.php?page=deppartement');
}
  echo "
  <div class=\"container-fluid mt-5 \">
  <div class=\"jumbotron\">
  <div class=\"card text-center text-white bg-dark\">
  <div class=\"card-header\" style=\"background-color:#242526;\">Departement</div>
  <div class=\"card-body\" style=\"background-color:#18191a;\">
  <h2 class=\"text-center card-title\"></h2>
  <div class=\"row\">
    <div class=\"container\">
      <div class=\"text-center container-fluid\">
      <button type=\"button\" class=\"btn   d-inline btn-success mb-5\" data-bs-toggle=\"modal\" data-bs-target=\"#ajouter\">
      Ajouter Deppartement
    </button>
      <div class=\"col-3 position-relative top-50 start-50 translate-middle\">
      <form class=\"d-flex \" action=\"#\" method=\"POST\" >
      <input class=\"form-control me-2\"  name=\"search\" type=\"search\" placeholder=\"Search\" aria-label=\"Search\">
      <button class=\"btn btn-primary  \" type=\"submit\">Search</button>
    </form>
    </div>
    <!-- Modal -->
    <div class=\"modal fade\" id=\"ajouter\" tabindex=\"-1\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
      <div class=\"modal-dialog modal-dialog-centered\">
        <div class=\"modal-content modal-dark\" style=\"background-color:#242526;\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\" id=\"exampleModalLabel\">Ajouter Deppartement</h5>
            <button type=\"button\" class=\"btn-close btn-close-white\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
          </div>
          <div class=\"modal-body\">
          <form method=\"POST\" action=\"deppartement/ajouter_deppartement.php\">
          <div class=\"input-group mb-3\">
          <span class=\"input-group-text\" id=\"basic-addon1\">Nom</span>
          <input type=\"text\" class=\"form-control\" name=\"nom\" placeholder=\"nom\" aria-label=\"nom\" aria-describedby=\"basic-addon1\">
        </div>
        <div class=\"input-group mb-3\">
          <span class=\"input-group-text\" id=\"basic-addon1\">Chef</span>
          <input type=\"text\" class=\"form-control\" name=\"chef\" placeholder=\"Chef\" aria-label=\"chef\" aria-describedby=\"basic-addon1\">
        </div>
   
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-secondary\" data-bs-dismiss=\"modal\">Close</button>
            <input type=\"submit\" class=\"btn btn-success\" name=\"ajouter\"value=\"Ajouter\"/>
          </div>
          </form>
        </div>
      </div>
    </div>
</div>
      <div class=\"row\">
        <div class=\"col-md-2\"></div>
        <div class=\"col-md-12\">
          <table class=\"table\" style=\"border-raduis:0.3rem;\">
            <thead class=\"table text-white\" style=\"background-color:#242526;\">
              <th>Id</th>
              <th>Nom</th>
              <th>Chef</th>
              <th>Crud</th>
            </thead>
            <tbody class=\"table table-light\">
           
";
  while($row =  mysqli_fetch_array($result, MYSQLI_ASSOC)){  
    echo "<tr><th scope=\"row\" >" . $row['id'] . "</th><td>" . $row['nom'] . "</td><td>" . $row['chef'] . "</td><td><a href=\"./deppartement/page_de_modifier_deppartement.php?modifier=".$row['id']." \" target=\"_self\"  class=\"btn btn-outline-primary\" >Modifier</a> <a href=\"./deppartement/supprimer_deppartement.php?supprimer=".$row['id']."\" name=\"supprimer\" class=\"btn btn-outline-danger\">Supprimer</a></td></tr>"; 
  }
  echo " </tbody>
  </table>
</div>
</div>
</div>
</div>
</div>
<div class=\"card-footer text-muted\" style=\"background-color:#242526;\">
".$_SESSION['db']."
</div>
</div>
</div>
</div>



";
}
mysqli_close($conn);

?>