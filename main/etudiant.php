<?php
session_start();
if (isset($_SESSION['connecter']) && $_SESSION['connecter']){
$conn = mysqli_connect('localhost', $_SESSION['username'], $_SESSION['password'],'PROJET_ECOLE_JAVA_SQL',3306);
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
  }
  $sql=null;
  if(isset($_POST['search'])){
  $sql="select * from etudiant where  nom like '%".$_POST['search']."%';";
  }if($sql== null ) {
    $sql='select * from etudiant;';
  }
  $result = mysqli_query($conn,$sql); 
  if(mysqli_num_rows($result)==0){
    $_SESSION['alert']='chercher';
    header('location:./index.php?page=etudiant');
  }
  echo "
  <div class=\"container-fluid mt-5 \">
  <div class=\"jumbotron\">
  <div class=\"card text-center text-white bg-dark\">
  <div class=\"card-header\" style=\"background-color:#242526;\">Etudiants</div>
  <div class=\"card-body\" style=\"background-color:#18191a;\">
  <h2 class=\"text-center card-title\"></h2>
  <div class=\"row\">
    <div class=\"container\">
      <div class=\"text-center\">
      <div class=\"text-center container-fluid\">
      <button type=\"button\" class=\"btn   d-inline btn-success mb-5\" data-bs-toggle=\"modal\" data-bs-target=\"#ajouter\">      Ajouter etudiant
    </button>
    <div class=\"col-3 position-relative top-50 start-50 translate-middle\">
    <form class=\"d-flex \" action=\"#\" method=\"POST\" >
    <input class=\"form-control me-2\"  name=\"search\" type=\"search\" placeholder=\"Search\" aria-label=\"Search\">
    <button class=\"btn btn-primary  \" type=\"submit\">Search</button>
  </form>
  </div>
    <!-- Modal -->
    <div class=\"modal fade\" id=\"ajouter\" tabindex=\"-1\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
      <div class=\"modal-dialog modal-dialog-centered\">
        <div class=\"modal-content modal-dark\" style=\"background-color:#242526;\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\" id=\"exampleModalLabel\">Ajouter etudiant</h5>
            <button type=\"button\" class=\"btn-close btn-close-white\" data-bs-dismiss=\"modal\" aria-label=\"Close\"></button>
          </div>
          <div class=\"modal-body\">
          <form method=\"POST\" action=\"etudiant/ajouter_etudiant.php\" enctype=\"multipart/form-data\">
        <div class=\"input-group mb-3\">
          <span class=\"input-group-text\" id=\"basic-addon1\">Image</span>
          <input type=\"file\" class=\"form-control\" name=\"img\" placeholder=\"img\" aria-label=\"img\" aria-describedby=\"basic-addon1\">
        </div>
        <div class=\"input-group mb-3\">
          <span class=\"input-group-text\" id=\"basic-addon1\">Nom</span>
          <input type=\"text\" class=\"form-control\" name=\"nom\" placeholder=\"nom\" aria-label=\"nom\" pattern=\"[A-Za-z-' ']{1,32}\" aria-describedby=\"basic-addon1\">
        </div>
        <div class=\"input-group mb-3\">
          <span class=\"input-group-text\" id=\"basic-addon1\">telephone</span>
          <input type=\"text\" class=\"form-control\" name=\"telephone\" placeholder=\"telephone\" aria-label=\"telephone\" pattern=\"[0-9]{10}\" aria-describedby=\"basic-addon1\">
        </div>
        <div class=\"input-group mb-3\">
        <span class=\"input-group-text\" id=\"basic-addon1\">Email</span>
        <input type=\"email\" class=\"form-control\" name=\"Email\" placeholder=\"Email\" aria-label=\"Email\" aria-describedby=\"basic-addon1\">
        </div>
        <!-- combo-box -->
        <div class=\"input-group mb-3\">
        <label class=\"input-group-text\" for=\"options\">options</label>
        <select class=\"form-select\"  name=\"options\" id=\"options\">
          <option selected>options...</option>";
          $etud_opt=mysqli_query($conn,"select id,nom from options ORDER BY id ASC ;");
          while($options =  mysqli_fetch_array($etud_opt, MYSQLI_ASSOC)){
              $valeur=$options['id'].' '.$options['nom'];
              $output = strtok($valeur,  ' ');
              echo "<option value=\"".$output."\">".$valeur."</option>
           ";
          }
        echo"
          </select>
      </div>
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-secondary\" data-bs-dismiss=\"modal\">Close</button>
            <input type=\"submit\" class=\"btn btn-success\" name=\"ajouter\"value=\"Ajouter\"/>
          </div>
          </form>
        </div>
      </div>
    </div>
</div>
      <div class=\"row\">
        <div class=\"col-md-2\"></div>
        <div class=\"col-md-12\">
          <table class=\"table\" style=\"border-raduis:0.3rem;\">
            <thead class=\"table text-white\" style=\"background-color:#242526;\">
              <th>Id</th>
              <th>image</th>
              <th>Nom</th>
              <th>telephone</th>
              <th>options</th>
              <th>Email</th>
              <th>Action</th>
            </thead>
            <tbody class=\"table table-light\">
           
";
  while($row =  mysqli_fetch_array($result, MYSQLI_ASSOC)){  
    echo "<tr><td scope=\"row\" >" . $row['id'] . "</td><td scope=\"row\" > <img  width=\"50px\"class=\"img-fluid rounded-circle\"src=\"data:image/png;base64, ". base64_encode( $row['img'] ). "\" alt=\"img\"> </td><td>" . $row['nom'] ."</td><td>" . $row['telephone'] ."</td><td>" . $row['options'] . "</td><td>" . $row['Email'] . "</td><td><a href=\"./etudiant/page_de_modifier_etudiant.php?modifier=".$row['id']." \" target=\"_self\"  class=\"btn btn-outline-primary\" >Modifier</a> <a href=\"./etudiant/supprimer_etudiant.php?supprimer=".$row['id']."\" name=\"supprimer\" class=\"btn btn-outline-danger\">Supprimer</a></td></tr>"; 
  }
  echo " </tbody>
  </table>
</div>
</div>
</div>
</div>
</div>
<div class=\"card-footer text-muted\" style=\"background-color:#242526;\">
".$_SESSION['db']."
</div>
</div>
</div>
</div>



";
}
mysqli_close($conn);

?>